from django.contrib import admin
from .models import *
# Register your models here.

class UsuarioProyectoInLine(admin.TabularInline):
	model=Equipo
	extra=0
class UserStoryProyectoInline(admin.TabularInline):
	model=UserStoryProyecto
	extra=0
class SrintProyectoInline(admin.TabularInline):
	model=SprintProyecto
	extra=0
class ProyectoAdmin(admin.ModelAdmin):
	inlines=(UsuarioProyectoInLine,UserStoryProyectoInline,SrintProyectoInline,)

admin.site.register(Sprint)
admin.site.register(Rol)
admin.site.register(TipoUS)
admin.site.register(UserStory)
admin.site.register(Proyecto,ProyectoAdmin)
admin.site.register(UsuarioProyecto)









'''
admin.site.register(Rol)
admin.site.register(TipoUS)
admin.site.register(Sprint)
'''
# Generated by Django 2.2 on 2019-04-26 20:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('is2', '0002_tipous'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sprint',
            fields=[
                ('codigo', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=20)),
                ('descripcion', models.CharField(max_length=20)),
            ],
        ),
    ]

from django.db import models
from django.contrib.auth import get_user_model

# Create your models here.
class Rol(models.Model):
    codigo = models.CharField(primary_key=True, max_length=20)
    nombre = models.CharField(max_length=20)

    def __str__(self):
        return self.codigo+','+self.nombre

class TipoUS(models.Model):
    codigo = models.CharField(primary_key=True, max_length=20)
    nombre = models.CharField(max_length=20)
    descripcion = models.CharField(max_length=20)

    def __str__(self):
        return self.codigo+','+self.nombre

class Sprint(models.Model):
    codigo = models.CharField(primary_key=True, max_length=20)
    nombre = models.CharField(max_length=20)
    descripcion = models.CharField(max_length=20)

    def __str__(self):
        return self.codigo + ',' + self.nombre


class UserStory(models.Model):
    por_hacer = 'PH'
    haciendo = 'HA'
    hecho = 'HE'
    estados = ((por_hacer, 'Por Hacer'), (haciendo, 'Haciendo'), (hecho, 'Hecho'),)
    codigo = models.CharField(max_length=12, primary_key=True)
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=200)
    estado = models.CharField(max_length=2, choices=estados, default=por_hacer)
    encargado = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='encargado0')
    codTipoUS = models.ForeignKey(TipoUS, on_delete=models.CASCADE)
    usowner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='owner')

    def __str__(self):
        return self.codigo + ',' + self.nombre


class UsuarioProyecto(models.Model):
    codigo = models.CharField(max_length=12, primary_key=True)
    usuario = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='userRol')
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE, related_name='rolUser')

    def __str__(self):
        return str(self.usuario) + ',' + str(self.rol)


class Proyecto(models.Model):
    activo = 'A'
    inactivo = 'I'
    estados = ((activo, 'Activo'), (inactivo, 'Inactivo'),)
    codigo = models.CharField(max_length=12, primary_key=True)
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=200)
    fechaInicio = models.DateField(max_length=30)
    fechaEntrega = models.DateField(max_length=30)
    estado = models.CharField(max_length=2, choices=estados, default=activo)
    equipoDesarrollo = models.ManyToManyField(UsuarioProyecto, through='Equipo',
                                              through_fields=('proyectoU', 'usuarioP'), )
    ProductOwner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='Product Owner+')
    Backlog = models.ManyToManyField(UserStory, through='UserStoryProyecto',
                                     through_fields=('proyectoUS', 'userStoryP'), )
    sprintBacklog = models.ManyToManyField(Sprint, through='SprintProyecto', through_fields=('proyectoS', 'sprintP'), )

    def __str__(self):
        return self.codigo + ',' + self.nombre + ',' + self.estado

class Equipo(models.Model):
	usuarioP=models.ForeignKey(UsuarioProyecto,on_delete=models.CASCADE)
	proyectoU=models.ForeignKey(Proyecto,on_delete=models.CASCADE)
	def __str__(self):
		return 'Usuario'


class SprintProyecto(models.Model):
	sprintP=models.ForeignKey(Sprint,on_delete=models.CASCADE)
	proyectoS=models.ForeignKey(Proyecto,on_delete=models.CASCADE)
	def __str__(self):
		return  'Sprint'

class UserStoryProyecto(models.Model):
	userStoryP=models.ForeignKey(UserStory,on_delete=models.CASCADE)
	proyectoUS=models.ForeignKey(Proyecto,on_delete=models.CASCADE)
	def __str__(self):
		return 'User Story'